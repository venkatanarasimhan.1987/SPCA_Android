package com.example.venkata.spca_v2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Venkata on 2016-02-22.
 */
public class DetailsViewerCat extends Activity {
    private ArrayAdapter arrayAdapter = null;

    private String[] animal_name;private String[] age;
    private String[] animal_type;private String[] house_type;
    private String[] animal_gender;private String[] cost;
    private String[] animal_desc;private String[] pet_frndly;

    ListView listView;

    TextView tv_name;TextView tv_cost;
    TextView tv_type;TextView tv_desc;
    TextView tv_gender;TextView tv_housetype;
    TextView tv_age;TextView tv_petfrndly;

    static String title1;static String title5;
    static String title2;static String title6;
    static String title7;static String title4;
    static String title3;static String title8;

    String httpPostVal;
    static byte[] data;
    ImageView imgViewer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailsviewer);

        tv_name = (TextView) findViewById(R.id.tv_name);

        tv_type = (TextView) findViewById(R.id.tv_type);

        tv_gender = (TextView) findViewById(R.id.tv_gender);

        tv_age = (TextView) findViewById(R.id.tv_age);

        tv_cost = (TextView) findViewById(R.id.tv_cost);

        tv_desc = (TextView) findViewById(R.id.tv_desc);

        tv_housetype = (TextView) findViewById(R.id.tv_housetype);

        tv_petfrndly = (TextView) findViewById(R.id.tv_petfrndly);

        imgViewer = (ImageView) findViewById(R.id.iv_image);

        AnimalCatViewer obj = new AnimalCatViewer();
        httpPostVal= obj.getHttpPostVal();

        String urlDetailsViewer = "http://ec2-52-26-121-62.us-west-2.compute.amazonaws.com:3000/animals/select/animalname/"+httpPostVal+"";
        new AsyncHttpTask().execute(urlDetailsViewer);

        System.out.println("Clicking the butt");

        String urlImageViewer = "http://ec2-52-26-121-62.us-west-2.compute.amazonaws.com:3000/animals/select/animalname/image/"+httpPostVal+"";
        new AsyncHttpTaskImg().execute(urlImageViewer);

        Toast.makeText(getBaseContext(), "Gathering Details", Toast.LENGTH_LONG).show();

    }
    //Do not Modify or change.
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        ProgressDialog dialog = new ProgressDialog(DetailsViewerCat.this);
        @Override
        protected void onPreExecute() {
            // what to do before background task
            dialog.setTitle("FETCHING AVAILABLE DOG'S");
            dialog.setMessage("Please Wait While We Fetch the Data."+
                    "This may take a few moments.");
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                 /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
                urlConnection.setRequestMethod("GET");
                int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if (statusCode ==  200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = convertInputStreamToString(inputStream);

                    parseResult(response);
                    Log.i("The response is:", response);
                    result = 1; // Successful
                }else{
                    result = 0; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d("TAG", e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPostExecute(Integer result) {
            /* Download complete. Lets update UI */
            dialog.dismiss();
            if (result == 1) {

                arrayAdapter = new ArrayAdapter(DetailsViewerCat.this, android.R.layout.simple_list_item_1, animal_name);

                tv_name.setText("Name:"+title1);
                tv_type.setText("Type:"+title2);
                tv_gender.setText("Gender"+title3);
                tv_age.setText("Age"+title4);
                tv_cost.setText("Cost:"+title5);
                tv_desc.setText("Description :"+title6);
                tv_housetype.setText("House type: "+title7);
                tv_petfrndly.setText("Pet Friendly: "+title8);


            } else {
                Log.e("TAG", "Failed to fetch data!");
            }
        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }

            /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

        String Result;

        private void parseResult(String result) {
            try{
                JSONObject response = new JSONObject(result);
                JSONArray posts = response.optJSONArray("list");
                animal_name = new String[posts.length()];
                animal_type = new String[posts.length()];
                animal_gender = new String[posts.length()];
                age = new String[posts.length()];
                animal_desc = new String[posts.length()];
                house_type = new String[posts.length()];
                cost = new String[posts.length()];
                pet_frndly = new String[posts.length()];

                for(int i=0; i< posts.length();i++ ){
                    JSONObject post = posts.optJSONObject(i);
                    title1 = post.optString("animal_name");
                    title2 = post.optString("animal_type");
                    title3 = post.optString("animal_gender");
                    title4 = post.optString("age");
                    title5 = post.optString("cost");
                    title6 = post.optString("animal_desc");
                    title7 = post.optString("house_type");
                    title8 = post.optString("pet_frndly");

                    animal_name[i] = title1;
                    animal_type[i] = title2;
                    animal_gender[i] = title3;
                    age[i] = title4;
                    cost[i] = title5;
                    animal_desc[i] = title6;
                    house_type[i] = title7;
                    pet_frndly[i] = title8;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }
    //Do not Modify or change.

    //For image alone:
    public class AsyncHttpTaskImg extends AsyncTask<String, Void, Integer> {


        @Override
        protected Integer doInBackground(String... params) {
            System.out.println("Do In Bkgnd ---> 1");
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                 /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
                urlConnection.setRequestMethod("GET");
                int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if (statusCode ==  200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = convertInputStreamToString(inputStream);
                    System.out.println("parseResult:--->");
                    parseResult(response);
                    result = 1; // Successful
                }else{
                    result = 0; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d("TAG", e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPostExecute(Integer result) {
            /* Download complete. Lets update UI */
            System.out.println("On Post Exec ---> 1");
            if (result == 1) {

                arrayAdapter = new ArrayAdapter(DetailsViewerCat.this, android.R.layout.simple_list_item_1, animal_name);

            } else {
                Log.e("TAG - PostExecImg", "Failed to fetch data!");
            }
        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            System.out.println("convertInputStreamToString ---> 1");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }

            /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

        String Result;

        private void parseResult(String result) {

            try{
                JSONObject response = new JSONObject(result);
                JSONArray posts = response.optJSONArray("imagelist");
                String ArrayFull = posts.toString();

                String test = ArrayFull.substring(10);
                test = test.substring(0,test.length()-2);
                String[] ImgbyteArray = test.substring(1, test.length() - 1).split(","); //returns all values within the [....]

                for(int i = 0; i<ImgbyteArray.length;i++){
                    if(i==ImgbyteArray.length-1){
                        System.out.print(ImgbyteArray[i]);
                    }
                    else{
                        System.out.print(ImgbyteArray[i]+",");
                    }
                }
                data = new byte[ImgbyteArray.length];
                for(int i =0; i<ImgbyteArray.length;i++){
                    int actualvalue = Integer.parseInt((ImgbyteArray[i]));
                    byte byteValue = 0;
                    if(actualvalue>127){
                        byteValue = (byte) (actualvalue-256);
                    }
                    else{
                        byteValue = (byte) (actualvalue);
                    }
                    data[i] = byteValue;
                }

                for(int i = 0; i<data.length;i++){
                    System.out.print(data[i]+",");
                }
                Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length);
                DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);

                imgViewer.setMinimumHeight(dm.heightPixels);
                imgViewer.setMinimumWidth(dm.widthPixels);
                imgViewer.setImageBitmap(bm);

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }


}
