package com.example.venkata.spca_v2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Venkata on 2015-12-25.
 */
public class AnimalCatViewer extends AppCompatActivity {

    //URL to get JSON Array
    private static String url = "http://ec2-52-26-121-62.us-west-2.compute.amazonaws.com:3000/animals/select/cats";

    private ArrayAdapter arrayAdapter = null;

    private String[] animal_name;
    private String[] animal_type;
    private String[] animal_gender;
    private String[] animal_desc;
    private String[] age;
    private String[] house_type;
    private String[] pet_frndly;
    private String[] cost;

    ListView listView;

    String title1;
    String title2;
    String title3;
    String title4;
    String title5;
    String title6;
    String title7;
    String title8;

    public static String httpPostVal;

    public String getHttpPostVal() {
        return httpPostVal;
    }

    public void setHttpPostVal(String httpPostVal) {
        this.httpPostVal = httpPostVal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat_viewer);

        final Context context = this;

        final Button getLatestList = (Button) findViewById(R.id.btngetList);

        getLatestList.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                //Toast.makeText(getBaseContext(), "Please Wait While We Fetch the Data....This may take a few moments", Toast.LENGTH_LONG).show();

                listView = (ListView) findViewById(R.id.listView);
                new AsyncHttpTask().execute(url);

                getLatestList.setEnabled(false);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            final int arg2, long arg3) {
                        Log.i("The actual value is: ", listView.getAdapter().getItem(arg2).toString());

                        httpPostVal = listView.getAdapter().getItem(arg2).toString();
                        setHttpPostVal(httpPostVal);

                        Intent intent = new Intent(context, DetailsViewerCat.class);
                        startActivity(intent);
                    }

                });
                getLatestList.setEnabled(true);
            }

        });

    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {
        ProgressDialog dialog = new ProgressDialog(AnimalCatViewer.this);


        @Override
        protected void onPreExecute() {
            // what to do before background task
            dialog.setTitle("FETCHING AVAILABLE CAT'S");
            dialog.setMessage("Please Wait While We Fetch the Data....This may take a few moments");
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                 /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
                urlConnection.setRequestMethod("GET");
                int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if (statusCode ==  200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = convertInputStreamToString(inputStream);
                    parseResult(response);
                    result = 1; // Successful
                }else{
                    result = 0; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d("TAG", e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPostExecute(Integer result) {
            /* Download complete. Lets update UI */
            dialog.dismiss();
            if (result == 1) {
                arrayAdapter = new ArrayAdapter(AnimalCatViewer.this, android.R.layout.simple_list_item_1, animal_name);
                listView.setAdapter(arrayAdapter);
            } else {
                Log.e("TAG", "Failed to fetch data!");
            }
        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }

            /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }


        private void parseResult(String result) {
            try{
                JSONObject response = new JSONObject(result);
                JSONArray posts = response.optJSONArray("list");
                animal_name = new String[posts.length()];
                animal_type = new String[posts.length()];
                animal_gender = new String[posts.length()];
                age = new String[posts.length()];
                animal_desc = new String[posts.length()];
                house_type = new String[posts.length()];
                pet_frndly = new String[posts.length()];
                cost = new String[posts.length()];

                for(int i=0; i< posts.length();i++ ){
                    JSONObject post = posts.optJSONObject(i);
                    title1 = post.optString("animal_name");
                    title2 = post.optString("animal_type");
                    title3 = post.optString("animal_gender");
                    title4 = post.optString("age");
                    title5 = post.optString("animal_desc");
                    title6 = post.optString("house_type");
                    title7 = post.optString("pet_frndly");
                    title8 = post.optString("cost");

                    animal_name[i] = title1;
                    animal_type[i] = title2;
                    animal_gender[i] = title3;
                    age[i] = title4;
                    animal_desc[i] = title5;
                    house_type[i] = title6;
                    pet_frndly[i] = title7;
                    cost[i] = title8;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }



}
