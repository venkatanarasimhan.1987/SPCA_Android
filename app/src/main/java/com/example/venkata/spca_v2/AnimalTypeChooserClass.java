package com.example.venkata.spca_v2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by Venkata on 2015-12-24.
 */
public class AnimalTypeChooserClass  extends Activity {

    ImageButton btn_DogChooser;
    ImageButton btn_CatChooser;
    ImageButton btn_ExoticChooser;

    //URL to get JSON Array
    private static String url = "http://ec2-52-26-121-62.us-west-2.compute.amazonaws.com:3000/animals/select/dogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_animal_chooser);
        addListenerOnButton();
    }

    public void addListenerOnButton() {

        final Context context = this;

        btn_DogChooser = (ImageButton) findViewById(R.id.btn_DogChooser);
        btn_CatChooser = (ImageButton) findViewById(R.id.btn_CatChooser);
        btn_ExoticChooser = (ImageButton) findViewById(R.id.btn_ExoticChooser);



        //Swapping between the different pages on login (i.e.) swapping between FRENCH and ENGLISH.
        //SOURCE: http://stackoverflow.com/questions/16469355/if-one-button-press-does-something-other-buttons-pressed-should-do-something-el
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(btn_DogChooser)) {
                    Intent intent = new Intent(context, AnimalDogViewer.class);
                    startActivity(intent);
                } else if (v.equals(btn_CatChooser)){
                    Intent intent = new Intent(context, AnimalCatViewer.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(context, AnimalExoticViewer.class);
                    startActivity(intent);
                }
            }
        };

        btn_DogChooser.setOnClickListener(listener);
        btn_CatChooser.setOnClickListener(listener);
        btn_ExoticChooser.setOnClickListener(listener);
    }
}
