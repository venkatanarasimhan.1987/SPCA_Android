package com.example.venkata.spca_v2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


public class LoginPage extends AppCompatActivity {

    ImageButton btn_LoginPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        addListenerOnButton();
    }


    public void addListenerOnButton() {

        final Context context = this;

        btn_LoginPage = (ImageButton) findViewById(R.id.btn_LoginPage);

        //Swapping between the different pages on login (i.e.) swapping between FRENCH and ENGLISH.
        //SOURCE: http://stackoverflow.com/questions/16469355/if-one-button-press-does-something-other-buttons-pressed-should-do-something-el
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(btn_LoginPage)) {
                    Intent intent = new Intent(context, AnimalTypeChooserClass.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(context, AnimalTypeChooserClass.class);
                    startActivity(intent);
                }
            }
        };

        btn_LoginPage.setOnClickListener(listener);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
